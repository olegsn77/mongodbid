<?php
namespace app\components\behaviors;
 
 
use yii\behaviors\AttributeBehavior;
use yii\base\ErrorException;
use yii\db\BaseActiveRecord;
use Yii;
use yii\helpers\Json;
use yii\mongodb\file\ActiveRecord as MongoGridFsAR;
use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper; 
use yii\mongodb\Query;

class MongoIdBehavior extends AttributeBehavior
{
    public $createdAtAttribute = 'id';
    public $collection = 'category';
    public $value;

    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->createdAtAttribute]
            ];
        }
    }

    protected function getValue($event)
    {
        if ($this->value === null) {
	        $query = new Query;
	        
			$rows = $query->from($this->collection)->all();
			
			if (!empty($rows)) {
				foreach ($rows as $row) {
					if ($row['id'] == null) {
						$row['id'] = 1;
					} else {
						$row['id'] = (string)$row['id'] + 1;
					}
				}
				$id = $row['id'];
				
			} else {
				
			    $id = 1;
			}
            return $id;
        }
        return parent::getValue($event);
    }

    public function touch($attribute)
    {
        /* @var $owner BaseActiveRecord */
        $owner = $this->owner;
        if ($owner->getIsNewRecord()) {
            throw new InvalidCallException('Updating id is not possible on a new record.');
        }
        $owner->updateAttributes(array_fill_keys((array) $attribute, $this->getValue(null)));
    }
	
}

